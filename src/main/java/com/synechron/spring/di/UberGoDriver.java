package com.synechron.spring.di;

import org.springframework.stereotype.Component;

//@Component
public class UberGoDriver implements Commute{

    public void commute(String startLocation, String destination) {
        System.out.println("Commuting from "+ startLocation+ " to "+ destination+ " using Uber Go");
    }
}