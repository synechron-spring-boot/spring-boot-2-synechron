package com.synechron.spring.di;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component("uberPassenger")
@Scope("singleton")
public class Passenger {

    private Commute car;
    @Autowired
    public Passenger (@Qualifier("commute") Commute car){
        this.car = car;
    }

    public void travel (String source, String destination){
        this.car.commute(source, destination);
    }

    @PostConstruct
    public void initialization(){
        System.out.println("Some custom initialization logic goes here");
    }

    @PreDestroy
    public void tearDown(){
        System.out.println("Customer tear down logic");
    }

}