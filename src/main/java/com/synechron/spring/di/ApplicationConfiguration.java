package com.synechron.spring.di;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

//@Component
@Configuration
public class ApplicationConfiguration {

    @Bean
    public Commute commute(){
        return new UberPrimeDriver();
    }
}