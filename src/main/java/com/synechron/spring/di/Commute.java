package com.synechron.spring.di;

public interface Commute {

    void commute(String startLocation, String destination);
}