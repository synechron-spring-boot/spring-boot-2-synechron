package com.synechron.spring.client;

import com.synechron.spring.di.Passenger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UberExperience {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
        Passenger passenger1 = applicationContext.getBean("uberPassenger", Passenger.class);
        Passenger passenger2 = applicationContext.getBean("uberPassenger", Passenger.class);
        System.out.println("is passenger1 same as passenger 2 "+ (passenger1 == passenger2));
        passenger1.travel("Koramangala", "WhiteField");
        passenger1.travel("RR Nagar", "Bannerghatta");
        passenger1.travel("Jayanagar", "JP-Nagar");


        ((AbstractApplicationContext)applicationContext).registerShutdownHook();
    }
}