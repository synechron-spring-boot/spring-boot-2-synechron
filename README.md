- Imperative 
   - How to do along with what to do 
   - Lot of control 
  ```
   for ( int i = 0; i < 10; i ++ ){
       System.out.println(array[i]);
   }
   ```

- Declarative programming
```
   for (String name: names){
       System.out.println(name);
   }
```
   - What to do and the system will figure out how to do 
   - Not have control of the inner workings 
   ```
   select * from orders where order_price > 20000 group by order by limit 4 
   ls -all | wc -l > files_metadata.txt  
   ```


- Spring Framework - Plumbing 
  - Dependency Injection 
  - Aspect Oriented Programming 
  
- Dependency Injection
  - XML way (old way)
  - Annotation 
  - Java Configuration
  
- Life cycle of Spring bean
    - Initialization - Called once
    - Service - Called during the method invocation
    - Destruction    - Called once
   - XML 
        ```
          <bean id='' class='' init-method='' destroy-method=''
        ```   
   - Annotation
        ```xml
        @PostConstuct
        @PreDestroy
        ```     
     
 - Scopes of Beans
    - Singleton - default scope
       - One instance for the application context
    - Prototype - Creates an instance for every invocation
    - Request, Session and application (only for web application)     
    - XML 
     ```xml
       <bean id="..." scope="singleton" />
     ```
    - Annotation
      ```xml
       @Scope("singleton")
      ``` 
- Stereotype Annotations
   - @ Configuration 
   - @ Service 
   - @ Repository
   - @ Controller
