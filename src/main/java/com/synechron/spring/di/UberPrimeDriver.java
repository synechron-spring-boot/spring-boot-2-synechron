package com.synechron.spring.di;

import org.springframework.stereotype.Component;

//@Component
public class UberPrimeDriver implements Commute {

    public void commute(String startLocation, String destination) {
        System.out.println("Commuting from "+ startLocation+ " to "+ destination+ " using Uber Prime");
    }
}